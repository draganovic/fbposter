using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FBPoster.Helpers;
using FBPoster.Models;
using Microsoft.EntityFrameworkCore;

namespace FBPoster.Data
{
    public class PostsRepository: IPostsRepository
    {
        private readonly DataContext _dataContext;

        public PostsRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Add<T>(T entity) where T : class
        {
            _dataContext.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _dataContext.Remove(entity);
        }

        public async Task<bool> SaveAll()
        {
            return await _dataContext.SaveChangesAsync() > 0;
        }

        public async Task<PagedList<Post>> GetPosts(PostParams postParams)
        {
            var posts = _dataContext.Posts.OrderByDescending(u => u.Created).AsQueryable();
            
            return await PagedList<Post>.CreateAsync(posts, postParams.page, postParams.PageSize);
        }

        public async Task<Post> GetPost(int id)
        {
            var post = await _dataContext.Posts.FindAsync(id);
            return post;
        }

        public async Task<List<Post>> GetReadyPosts()
        {
            return _dataContext.Posts
                .Where(p => p.Done == false)
                .Where(p => p.HasError == false)
                .Where(p => p.PublishOn < DateTime.Now)
                .Where(p=> p.SaveNow == false)
                .ToList();
//            var posts = _dataContext.Posts.OrderByDescending(u => u.Created).AsQueryable();
//            return await PagedList<Post>.CreateAsync(posts, 0, 10);
        }

        public async Task<List<Post>> GetPostsToSave()
        {
            return _dataContext.Posts.Where(p => p.Done == false).Where(p => p.HasError == false).Where(p => p.SaveNow).ToList();
        }

        public async Task<bool> Save()
        {
            return _dataContext.SaveChanges() > 0;
        }
    }
}