using System.Collections.Generic;
using System.Threading.Tasks;
using FBPoster.Helpers;
using FBPoster.Models;

namespace FBPoster.Data
{
    public interface IPostsRepository
    {
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<PagedList<Post>> GetPosts(PostParams postParams);
        Task<Post> GetPost(int id);
        Task<List<Post>> GetReadyPosts();
        Task<List<Post>> GetPostsToSave();
        Task<bool> Save();
    }
}