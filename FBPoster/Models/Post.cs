using System;

namespace FBPoster.Models
{
    public class Post
    {
        public int Id { get; set; }
        public string PageID { get; set; }
        public DateTime Created { get; set; }
        public DateTime PublishOn { get; set; }
        public string Content { get; set; }
        public string ImgUrl { get; set; }
        public string Url { get; set; }

        public bool HasError { get; set; }
        public String ErrorMsg { get; set; }

        public bool Done { get; set; }
        public bool SaveNow { get; set; }
    }
}