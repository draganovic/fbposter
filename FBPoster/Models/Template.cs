using System;
using CsvHelper.Configuration.Attributes;

namespace FBPoster.Models
{
    public class Template
    {
        [Name("Page ID")]
        public string PageID { get; set; }
        [Name("PublishOn")]
        public DateTime PublishOn { get; set; }
        [Name("Content")]
        public string Content { get; set; }
        [Name("Img URL")]
        public string ImgUrl { get; set; }
        [Name("URL")]
        public string Url { get; set; }
        [Name("SaveNow")] 
        public bool SaveNow { get; set; }
    }
}