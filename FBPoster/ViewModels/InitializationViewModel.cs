namespace FBPoster.ViewModels
{
    public class InitializationViewModel
    {
        public FacebookLoginViewModel FacebookLogin { get; set; }
        public bool LoggedIn { get; set; }
        public string LoginError { get; set; }
        public bool HasError { get; set; }
    }
}