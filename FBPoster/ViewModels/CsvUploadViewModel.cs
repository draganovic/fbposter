using System.Collections.Generic;
using FBPoster.Models;

namespace FBPoster.ViewModels
{
    public class CsvUploadViewModel
    {
        public List<PostForUpload> Posts { get; set; } = new List<PostForUpload>();
        public bool HasError { get; set; } = false;
        public List<int> ErrorLines { get; set; } = new List<int>();
    }
}