using System.ComponentModel.DataAnnotations;

namespace FBPoster.ViewModels
{
    public class FacebookLoginViewModel
    {
        public string Username { get; set; }  
   
        [DataType(DataType.Password)]
        public string Password { get; set; }  
    }
}