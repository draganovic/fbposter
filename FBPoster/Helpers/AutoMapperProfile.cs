using AutoMapper;
using FBPoster.Models;
using FBPoster.ViewModels;

namespace FBPoster.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<PostForUpload, Post>();
            CreateMap<Post, PostForUpload>();
        }
    }
}