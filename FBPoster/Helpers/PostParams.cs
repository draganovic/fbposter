namespace FBPoster.Helpers
{
    public class PostParams
    {
        private const int MaxPageSize = 50; 
        public int page { get; set; } = 1;
        private int pageSize = 10;
        public int PageSize {
            get { return pageSize; }
            set { pageSize = (value > MaxPageSize) ? MaxPageSize : value; }
        }
    }
}