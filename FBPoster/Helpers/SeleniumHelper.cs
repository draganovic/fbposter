using System;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using FBPoster.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;

namespace FBPoster.Helpers
{
    public interface ISeleniumHelper
    {
        bool Running { get; set; }
        IWebDriver Driver { get; set; }
        bool LoggedIn { get; set; }
        bool NeedAuth { get; set; }
        IWebDriver NewInstance();
        void LogIn(string user, string pass);
        bool IsElementPresent(By by);
        string Post(Post post);
        string Save(Post post);
    }

    public class SeleniumHelper : ISeleniumHelper
    {
        public bool Running { get; set; }
        public IWebDriver Driver { get; set; } = null;
        public bool LoggedIn { get; set; }
        public bool NeedAuth { get; set; }

        public IWebDriver NewInstance()
        {
            var path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            path = path.Substring(RuntimeInformation.IsOSPlatform(OSPlatform.OSX) ? 5 : 6);
            var service = FirefoxDriverService.CreateDefaultService(path,
                RuntimeInformation.IsOSPlatform(OSPlatform.OSX) ? "geckodriver" : "geckodriver.exe");
//            ChromeDriverService service = ChromeDriverService.CreateDefaultService(path, RuntimeInformation.IsOSPlatform(OSPlatform.OSX) ? "chromedriver" : "chromedriver.exe");
            service.HideCommandPromptWindow = true;
//
//            
//            var op = new FirefoxOptions();
//            var profileManager = new FirefoxProfileManager();
//            FirefoxProfile profile = profileManager.GetProfile("Default");
//            op.Profile = profile;

//            DesiredCapabilities capability = DesiredCapabilities.Firefox();
//            Uri url = new Uri("http://REMOTE_IP:4545/wd/hub");
//            IWebDriver driver = new RemoteWebDriver(url, capability);

            var op = new FirefoxOptions();
//            op.AddArgument("--no-sandbox");
//            op.AddArguments(
//                $"--user-data-dir={Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)}" +
//                $"{(RuntimeInformation.IsOSPlatform(OSPlatform.OSX) ? "/Library/Application Support/Google/Chrome/" : "/AppData/Local/Google/Chrome/User Data")}"); //C://Users/Riadh/
////                $"{(RuntimeInformation.IsOSPlatform(OSPlatform.OSX) ? "/Library/Application Support/com.operasoftware.Opera" : "/AppData/Local/Google/Chrome/User Data")}");//C://Users/Riadh/
//            op.AddArgument(
//                "--user-agent=Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36");
//            op.AddArgument("--disable-extensions");
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding.GetEncoding(437);
//            
//            var profileManager = new FirefoxProfileManager();
//            var profile = profileManager.GetProfile("default");
            var profile = new FirefoxProfile();
            profile.SetPreference("general.useragent.override",
                "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36");
//            op.Profile.DeleteAfterUse = true;
            op.Profile = profile;
            IWebDriver driver = new FirefoxDriver("/Users/Draganovic/RiderProjects/FBPoster/FBPoster/bin/Debug/netcoreapp2.1/",op);

//            driver.Manage().Window.FullScreen();

            return driver;
        }
        
        
        public static void HoverAndClick(IWebDriver driver,IWebElement elementToClick) {
            Actions action = new Actions(driver);
            action.MoveToElement(elementToClick).Click(elementToClick).Build().Perform();
        }

        public void LogIn(string user, string pass)
        {
            Running = true;
            Driver = NewInstance();
            Driver.Navigate().GoToUrl("https://m.facebook.com/");

            var logoutElement = By.XPath("//a[contains(.,'Logout')]");
            WebDriverWait waiter = new WebDriverWait(Driver, TimeSpan.FromSeconds(3));
            try
            {
                waiter.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(logoutElement));
            }
            catch (WebDriverTimeoutException)
            {
                Random randomSleep = new Random();
                
                Driver.FindElement(By.XPath("//input[@name='email']")).Click();
                foreach (var uCar in user)
                {
                    Driver.FindElement(By.XPath("//input[@name='email']")).SendKeys(uCar.ToString());
                    var next = randomSleep.Next(100, 400);
                    Thread.Sleep(next);
                }

                Driver.FindElement(By.XPath("//input[@name='email']")).Click();
                foreach (var uPass in pass)
                {
                    Driver.FindElement(By.XPath("//input[@name='pass']")).SendKeys(uPass.ToString());
                    var next = randomSleep.Next(50, 400);
                    Thread.Sleep(next);
                }
//                Actions builder = new Actions(Driver);
//                builder.MoveToElement(Driver.FindElement(By.XPath("//input[@name='email']"))).Click().SendKeys(user).Perform();
//                builder.MoveToElement(Driver.FindElement(By.XPath("//input[@name='pass']"))).Click().SendKeys(pass).Perform();
                
//                Driver.FindElement(By.XPath("//input[@name='pass']")).Submit();
                HoverAndClick(Driver,Driver.FindElement(By.XPath("//button[@name='login']")));
            }


            if (IsElementPresent(By.XPath("//input[@value='OK']")))
            {
                Driver.FindElement(By.XPath("//input[@value='OK']")).Click();
                LoggedIn = true;
                Running = false;
                return;
            }

            if (IsElementPresent(By.XPath("//input[@name='approvals_code']")))
            {
                NeedAuth = true;
                Running = false;
                return;
            }

            if (IsElementPresent(By.XPath("//div[@id='MComposer']")))
            {
                LoggedIn = true;
                Running = false;
                return;
            }


            Driver.Quit();
            Driver = null;
            Running = false;
        }


        public bool IsElementPresent(By by)
        {
            WebDriverWait waiter = new WebDriverWait(Driver, TimeSpan.FromSeconds(5));
            try
            {
                waiter.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public string Post(Post post)
        {
            try
            {
                // Go to the pages pages.
                Driver.Navigate().GoToUrl($"https://m.facebook.com/{post.PageID}");

                // Verification that the page ID is valid and you are admin of the page
                // Click on publish to go to the composer page.
                try
                {
                    Driver.FindElement(By.XPath("//a[contains(.,'Publish')]")).Click();
                }
                catch (Exception e)
                {
                    return
                        "Page ID is incorrect or you are not admin of this page, Please recheck it and edit this post.";
                }


                // Write the text content of the Post.
                if (post.ImgUrl != null)
                {
                    try
                    {
                        Driver.FindElement(By.XPath("//input[@value='Photo']")).Click();

                        string localFilename = @"C:\FBposterDeploy\image.jpg";
                        if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                        {
                            localFilename = @"/Users/Draganovic/Pictures/image.png";
                        }

                        using (WebClient client = new WebClient())
                        {
                            client.DownloadFile(post.ImgUrl, localFilename);
                        }

                        var fileSelectEl = Driver.FindElement(By.XPath("//input[@name='file1']"));
                        fileSelectEl.SendKeys(localFilename);
                        Driver.FindElement(By.XPath("//input[@value='Preview']")).Click();
                    }
                    catch (Exception e)
                    {
                        return "Invalid image Url, Please make sure of it.";
                    }
                }


                var textAreaElementXpath = By.XPath("//textarea[@name='xc_message']");
                WebDriverWait waiter = new WebDriverWait(Driver, TimeSpan.FromSeconds(60));
                try
                {
                    waiter.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(textAreaElementXpath));
                }
                catch (WebDriverTimeoutException e)
                {
                    return "Couldn't find the message text area.";
                }

                var textAreaElement = Driver.FindElement(textAreaElementXpath);
                if (!String.IsNullOrEmpty(post.Content) && !String.IsNullOrWhiteSpace(post.Content))
                {
                    textAreaElement.SendKeys(post.Content);
                }

                if (!String.IsNullOrEmpty(post.Url) && !String.IsNullOrWhiteSpace(post.Url))
                {
                    textAreaElement.SendKeys(Keys.Enter + Keys.Enter + post.Url);
                }

                // Submit the form.
                Driver.FindElement(By.XPath("//input[@value='Post']")).Click();
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return "Done";
        }

        public string Save(Post post)
        {
            try
            {
                // Go to the pages pages.
                Driver.Navigate().GoToUrl($"https://m.facebook.com/{post.PageID}");

                // Verification that the page ID is valid and you are admin of the page
                // Click on publish to go to the composer page.
                try
                {
                    Driver.FindElement(By.XPath("//a[contains(.,'Publish')]")).Click();
                }
                catch (Exception e)
                {
                    return
                        "Page ID is incorrect or you are not admin of this page, Please recheck it and edit this post.";
                }


                // Write the text content of the Post.
                if (post.ImgUrl != null)
                {
                    try
                    {
                        Driver.FindElement(By.XPath("//button[@title='Add a photo to post']")).Click();

                        string localFilename = @"C:\FBposterDeploy\image.jpg";
                        if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                        {
                            localFilename = @"/Users/Draganovic/Pictures/image.png";
                        }
                        // Download the Img.
                        using (WebClient client = new WebClient())
                        {
                            client.DownloadFile(post.ImgUrl, localFilename);
                        }

                        Driver.FindElement(By.XPath("//button[@title='Add a photo to post']")).Click();
                        
                        var process = new Process()
                        {
                            StartInfo = new ProcessStartInfo()
                            {
                                FileName = "java.exe",
                                Arguments = $"-jar RobotKeyboardTyping-1.0.jar {localFilename}",
                                RedirectStandardOutput = true,
                                UseShellExecute = false,
                                CreateNoWindow = true
                            }
                        };
                    }
                    catch (Exception e)
                    {
                        return "Invalid image Url, Please make sure of it.";
                    }
                }


                var textAreaElementXpath = By.XPath("//textarea[@name='xc_message']");
                WebDriverWait waiter = new WebDriverWait(Driver, TimeSpan.FromSeconds(60));
                try
                {
                    waiter.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(textAreaElementXpath));
                }
                catch (WebDriverTimeoutException e)
                {
                    return "Couldn't find the message text area.";
                }

                var textAreaElement = Driver.FindElement(textAreaElementXpath);
                if (!String.IsNullOrEmpty(post.Content) && !String.IsNullOrWhiteSpace(post.Content))
                {
                    textAreaElement.SendKeys(post.Content);
                }

                if (!String.IsNullOrEmpty(post.Url) && !String.IsNullOrWhiteSpace(post.Url))
                {
                    textAreaElement.SendKeys(Keys.Enter + Keys.Enter + post.Url);
                }

                // Submit the form.
                Driver.FindElement(By.XPath("//input[@value='Post']")).Click();
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return "Done";
        }
    }
}