using System;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using AutoMapper;
using FBPoster.Data;
using FBPoster.Helpers;
using FBPoster.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FBPoster.Controllers
{
    [Authorize]
    public class PostsController : Controller
    {
        private readonly IPostsRepository _postsRepository;
        private readonly IMapper _mapper;

        public PostsController(IPostsRepository postsRepository, IMapper mapper)
        {
            _postsRepository = postsRepository;
            _mapper = mapper;
        }

        // GET 
        public async Task<IActionResult> Index([FromQuery]PostParams postParams)
        {
            var posts = await _postsRepository.GetPosts(postParams);
            
            return View(posts);
        }

        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {
            var post = await _postsRepository.GetPost(id);
            if (post != null)
            {
                return View(post);
            }

            return RedirectToAction("Index", "Posts");
        }
            
        [HttpPost("Edit/{id}")]
        public async Task<IActionResult> Edit(int id,PostForUpload postForUpload)
        {
            var postFromRepo = await _postsRepository.GetPost(id);
            postFromRepo.Done = false;
            postFromRepo.HasError = false;
            postFromRepo.ErrorMsg = null;
            
            _mapper.Map(postForUpload, postFromRepo);

            if (await _postsRepository.SaveAll())
            {
                return RedirectToAction("Index","Posts");
            }

            return RedirectToAction("Edit", "Posts", new {id});
        }

        [HttpGet("Details/{id}")]
        public async Task<IActionResult> Details(int id)
        {
            var postFromRepo = await _postsRepository.GetPost(id);
            var postToReturn = _mapper.Map<PostForUpload>(postFromRepo);
            return View(postToReturn);
        }
        
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var postFromRepo = await _postsRepository.GetPost(id);

            if (postFromRepo == null)
            {
                return RedirectToAction("Index", "Posts");
            }
            _postsRepository.Delete(postFromRepo);
            
            if (await _postsRepository.SaveAll())
                return RedirectToAction("Index","Posts");
            return RedirectToAction("Index", "Posts");
        }
    }
}