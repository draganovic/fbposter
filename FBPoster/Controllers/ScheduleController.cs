using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Transactions;
using CsvHelper.Configuration.Attributes;
using FBPoster.Data;
using FBPoster.Models;
using FBPoster.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Identity.UI.Pages.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR.Protocol;
using Microsoft.CodeAnalysis.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Localization;
using Microsoft.Net.Http.Headers;

namespace FBPoster.Controllers
{
    [Authorize]
    public class ScheduleController : Controller
    {
        private readonly IPostsRepository _postsRepository;

        public ScheduleController(IPostsRepository postsRepository)
        {
            _postsRepository = postsRepository;
        }

        // GET
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("data.csv")]
        [Produces("text/csv")]
        public IActionResult GetDataAsCsv()
        {
            return Ok(DummyData());
        }


        [HttpPost]
        public async Task<IActionResult> Confirm(CsvUploadViewModel csvUploadViewModel)
        {
            foreach (var post in csvUploadViewModel.Posts)
            {
                _postsRepository.Add(
                    new Post
                    {
                        Content = post.Content,
                        PageID = post.PageID,
                        PublishOn = post.PublishOn,
                        Created = DateTime.Now,
                        ImgUrl = post.ImgUrl,
                        Url = post.Url,
                        SaveNow = post.SaveNow
                    });
            }

            await _postsRepository.SaveAll();

            return RedirectToAction("Index", "Posts");
        }


        private static IEnumerable<Template> DummyData()
        {
            return new List<Template>();
        }

        public async Task<IActionResult> UploadCsv(CsvImportDescription csvImportDescription)
        {
            var contentTypes = new List<string>();

            CsvUploadViewModel items = null;
            if (ModelState.IsValid)
            {
                if (csvImportDescription.File != null)
                    foreach (var file in csvImportDescription.File)
                    {
                        if (file.Length > 0)
                        {
                            var extension = Path.GetExtension(file.FileName);
                            if (!".csv".Equals(extension, StringComparison.OrdinalIgnoreCase))
                            {
                                return RedirectToAction("Index", "Schedule");
                            }
                            
                            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName
                                .ToString()
                                .Trim('"');
                            contentTypes.Add(file.ContentType);

                            var inputStream = file.OpenReadStream();
                            items = readStream(file.OpenReadStream());
                            if (items.Posts.Count.Equals(0))
                            {
                                return RedirectToAction("Index", "Schedule");
                            }
                        }
                    }
                else
                {
                    return RedirectToAction("Index", "Schedule");
                }
            }

            return View(items);
        }

        private CsvUploadViewModel readStream(Stream stream)
        {
            bool skipFirstLine = true;
            string csvDelimiter = ",";

//            List<PostForUpload> list = new List<PostForUpload>();
            CsvUploadViewModel csvUploadViewModel = new CsvUploadViewModel();

            var reader = new StreamReader(stream);

            int j = 0;
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                if (line != null)
                {
                    var values = line.Split(csvDelimiter.ToCharArray());
                    if (skipFirstLine)
                    {
                        skipFirstLine = false;
                    }
                    else
                    {
//                    var itemTypeInGeneric = csvUploadViewModel.Posts.GetType().GetTypeInfo().GenericTypeArguments[0];
                        var item = new PostForUpload();
                        var properties = item.GetType().GetProperties();
                        for (int i = 0; i < values.Length; i++)
                        {
                            try
                            {
//                                if (String.IsNullOrEmpty(values[i]) || String.IsNullOrWhiteSpace(values[i]))
//                                {
//                                    throw new Exception();
//                                }

                                if (properties[i].Name == "Page ID")
                                {
                                    if (String.IsNullOrEmpty(values[i]) || String.IsNullOrWhiteSpace(values[i]))
                                    {
                                        throw new Exception();
                                    }
                                } else if (properties[i].Name == "SaveNow")
                                {
                                    if (String.IsNullOrEmpty(values[i]) || String.IsNullOrWhiteSpace(values[i]))
                                    {
                                        throw new Exception();
                                    }

                                    if (values[i].ToUpper().Equals("YES"))
                                    {
                                        item.SaveNow = true;
                                    }
                                    else if (values[i].ToUpper().Equals("NO"))
                                    {
                                        item.SaveNow = false;
                                    }
                                    else
                                    {
                                        throw new Exception();
                                    }
                                } else if (properties[i].Name == "PublishOn")
                                {
                                    int secs = 00;
                                    int mnts = 00;
                                    int hours = 00;
                                    int days = 00;
                                    int mouths = 00;
                                    int years = 00;

                                    var spaceSplitDate = values[i].Split(" ");
                                    var slashSplitDate = spaceSplitDate[0].Split("/");

                                    if (slashSplitDate.Length > 3)
                                    {
                                        throw new Exception();
                                    }

                                    for (int k = 0; k < 3; k++)
                                    {
                                        if (slashSplitDate[k].Length > 2)
                                        {
                                            throw new Exception();
                                        }
                                    }

                                    var date = new DateTime();
                                    mouths = int.Parse(slashSplitDate[0]);
                                    days = int.Parse(slashSplitDate[1]);
                                    years = int.Parse(slashSplitDate[2]);

                                    if (mouths > 12 || days > 31 || years > 99)
                                    {
                                        throw new Exception();
                                    }

                                    date = date.AddMonths(mouths - 1);
                                    date = date.AddDays(days - 1);
                                    date = date.AddYears(2000 + years - 1);

                                    var time = spaceSplitDate[1].Split(":");

                                    if (time.Length > 3)
                                    {
                                        throw new Exception();
                                    }

                                    foreach (var valueTime in time)
                                    {
                                        if (valueTime.Length > 2)
                                        {
                                            throw new Exception();
                                        }
                                    }

                                    hours = int.Parse(time[0]);
                                    mnts = int.Parse(time[1]);
                                    try
                                    {
                                        secs = int.Parse(time[2]);
                                    }
                                    catch (Exception ignored)
                                    {
                                        // ignored
                                    }

                                    if (hours > 24 || mnts > 60 || secs > 60)
                                    {
                                        throw new Exception();
                                    }
                                    
                                    date = date.AddHours(hours);
                                    date = date.AddMinutes(mnts);
                                    date = date.AddSeconds(secs);


//                                    DateTime myDate = DateTime.ParseExact(values[i], "MM/dd/yyyy HH:mm",
//                                        System.Globalization.CultureInfo.InvariantCulture);
                                    item.PublishOn = date;
                                    Console.WriteLine(item.PublishOn);
                                }
                                else
                                {
                                    properties[i].SetValue(item,
                                        Convert.ChangeType(values[i], properties[i].PropertyType), null);
                                }
                            }
                            catch (Exception e)
                            {
                                Console.Error.WriteLine(e);
                                csvUploadViewModel.HasError = true;
                                csvUploadViewModel.ErrorLines.Add(j);
                            }
                        }

                        csvUploadViewModel.Posts.Add(item);
                        j++;
                    }
                }
            }

            return csvUploadViewModel;
        }
    }
}