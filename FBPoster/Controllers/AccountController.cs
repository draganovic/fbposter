using System;
using System.Threading.Tasks;
using FBPoster.Models;
using FBPoster.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FBPoster.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        
        // GET
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!model.RegistrationPassword.Equals("PwFBposterBrandy"))
                {
                    ModelState.AddModelError("RegistrationPassword","Registration password is incorrect");
                    return View();
                }
                
                var applicationUser = new ApplicationUser {UserName = model.Username};
                var createResult = await _userManager.CreateAsync(applicationUser, model.Password);
                if (createResult.Succeeded)
                {
                    await _signInManager.SignInAsync(applicationUser, false);
                    return RedirectToAction("Index", "Posts");
                }

                foreach (var error in createResult.Errors)
                {
                    ModelState.AddModelError("",error.Description);
                }
            }

            return View();
        }

        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Posts");
        }
        
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(loginViewModel.Username,loginViewModel.Password,loginViewModel.RememberMe,false);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Posts");
                }
                ModelState.AddModelError("", "Invalid Login Attempt.");
                return View();
            }
            
            return View();
        }
    }
}