using FBPoster.Helpers;
using FBPoster.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OpenQA.Selenium;

namespace FBPoster.Controllers
{
    [Authorize]
    public class SetUpController : Controller
    {
        private readonly ISeleniumHelper _seleniumHelper;

        // GET
        public SetUpController(ISeleniumHelper seleniumHelper)
        {
            _seleniumHelper = seleniumHelper;
        }

        public IActionResult Index()
        {
            InitializationViewModel model = new InitializationViewModel {LoggedIn = _seleniumHelper.LoggedIn};
            return View(model);
        }
        [HttpPost]
        public IActionResult Index(InitializationViewModel model)
        {
            if (ModelState.IsValid)
            {
                _seleniumHelper.LogIn(model.FacebookLogin.Username, model.FacebookLogin.Password);
                if (_seleniumHelper.NeedAuth)
                {
                    return RedirectToAction("Authentication", "SetUp");
                }

                if (_seleniumHelper.LoggedIn)
                {
                    return RedirectToAction("Index","Posts");
                }
            }

            ModelState.AddModelError("", "Invalid login attempt");
            return View(model);
        }

        public IActionResult Disconnect()
        {
            _seleniumHelper.Driver?.Quit();
            _seleniumHelper.LoggedIn = false;
            _seleniumHelper.Running = false;
            return RedirectToAction("Index", "SetUp");
        }
        
        
        public IActionResult Authentication()
        {
            if (_seleniumHelper.Driver == null)
                return RedirectToAction("Index", "SetUp");
            return View();
        }

        [HttpPost]
        public IActionResult Authentication(AuthenticationViewModel model)
        {
            if (!model.Code.Equals(""))
            {
                if (_seleniumHelper.Driver != null)
                {
                    _seleniumHelper.Running = true;
                    var codeInputElement = _seleniumHelper.Driver.FindElement(By.XPath("//input[@name='approvals_code']"));
                    codeInputElement.SendKeys(model.Code);
                    _seleniumHelper.Driver.FindElement(By.XPath("//input[@value='submit[Submit Code]']")).Click();
                    if (_seleniumHelper.IsElementPresent(By.XPath("//input[@value='submit[Submit Code]']")))
                    {
                        ModelState.AddModelError("", "Invalid Code");
                        return View(model);
                    }
                    _seleniumHelper.Driver.FindElement(By.XPath("//input[@value='Continue']")).Click();
                    if (_seleniumHelper.IsElementPresent(By.XPath("//a[contains(.,'Logout')]")))
                    {
                        _seleniumHelper.LoggedIn = true;
                        _seleniumHelper.Running = false;
                        return RedirectToAction("Index", "Posts");
                    }
                }
            }
            ModelState.AddModelError("", "Invalid Code");
            return View(model);
        }
    }
}