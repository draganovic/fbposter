using System;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using FBPoster.Data;
using FBPoster.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.Extensions.Logging;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace FBPoster.Services
{
    internal interface IScopedProcessingService
    {
        Task DoWork();
    }

    internal class ScopedProcessingService : IScopedProcessingService
    {
        private readonly ILogger _logger;
        private readonly IPostsRepository _postsRepository;
        private readonly ISeleniumHelper _seleniumHelper;

        public ScopedProcessingService(ILogger<ScopedProcessingService> logger,
            IPostsRepository postsRepository,
            ISeleniumHelper seleniumHelper)
        {
            _logger = logger;
            _postsRepository = postsRepository;
            _seleniumHelper = seleniumHelper;
        }

        public async Task DoWork()
        {
            _logger.LogInformation("Posting service is working.");

            try
            {
                Console.WriteLine($"Running: {_seleniumHelper.Running}, Logged in: {_seleniumHelper.LoggedIn}");
//                Console.WriteLine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)+"/Library/Application Support/Google/Chrome/");
                if (_seleniumHelper.LoggedIn && !_seleniumHelper.Running)
                {
                    _seleniumHelper.Running = true;
                    _logger.LogInformation("Posting started.");

                    var posts = await _postsRepository.GetReadyPosts();
                    var postsToSave = await _postsRepository.GetPostsToSave();
                    
                    
                    var driver = _seleniumHelper.Driver;
                    if (posts.Count.Equals(0) && postsToSave.Count.Equals(0))
                    {
                        Console.WriteLine("Refreshing");
                        driver.Navigate().Refresh();
                    }
                    else
                    {
                        foreach (var post in posts)
                        {
                            var result = _seleniumHelper.Post(post);
                            if (!result.Equals("Done"))
                            {
                                Console.WriteLine(result);
                                post.HasError = true;
                                post.ErrorMsg = result;
                            }
                            else
                            {
                                post.Done = true;
                            }
                        }

                        foreach (var post in postsToSave)
                        {
                            var result = _seleniumHelper.Save(post);
                            if (!result.Equals("Done"))
                            {
                                Console.WriteLine(result);
                                post.HasError = true;
                                post.ErrorMsg = result;
                            }
                            else
                            {
                                post.Done = true;
                            }
                        }

                        if (await _postsRepository.Save())
                        {
                            _seleniumHelper.Running = false;
                            _logger.LogInformation("Posting completed without errors.");
                        }
                        else
                        {
                            _seleniumHelper.Running = false;
                            _logger.LogInformation("Posting completed with errors.");
                        }
                    }
                    _seleniumHelper.Running = false;
                }
            }
            catch (Exception e)
            {
                _seleniumHelper.Running = false;
                Console.WriteLine(e);
            }
        }
    }
}